Gem::Specification.new do |s|
  s.name        = 'slothsay'
  s.license     = 'MIT'
  s.version     = '0.0.1'
  s.date        = '2014-03-25'
  s.summary     = "Slothsay"
  s.description = "Slothsay: slothsay hello"
  s.authors     = ["custard"]
  s.email       = 'custard@cpan.org'
  s.bindir      = 'bin'
  s.executables << 'slothsay'
  s.files       = [
    "bin/slothsay",
    "lib/cat.rb",
    "lib/catsay.rb",
    "lib/exceptions.rb",
    "cats/default.erb",
    "cats/test.erb"
  ]
  s.homepage    = 'https://github.com/CustardCat/slothsay'
end
